def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)

    # CTA base URLs
    config.add_route('forgot_password','/forgot_password')
    config.add_route('reset_password', '/reset_password/{token}')
    config.add_route('logout', '/logout')
    config.add_route('verify_email', '/verify_email')
    config.add_route('get_state', '/registration/get_state/')
    config.add_route('get_company_state', '/company_registration/get_state/')
    config.add_route('cta_registration', '/registration/{token}')
    # config.add_route('get_state', '/registration/get_state')

    config.add_route('company_registration','/company_registration/{token}')
    config.add_route('home', '/home')
    # config.add_route('home', '/home/{uuid}')
    config.add_route('link-expired', '/link-expired')
    config.add_route('reset_link_expired','/reset_link_expired')
    config.add_route('login', '/')
    config.add_route('profile_view', '/profile')
    config.add_route('profile.load_communication_preferences','/load_communication_preferences')
    config.add_route('profile.communication_preferences','/communication_preferences')
    config.add_route('profile.change_password', '/change_password')
    config.add_route('profile.edit_profile', '/edit_profile')

