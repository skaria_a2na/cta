import requests
import json


class MailgunClient:
    _api_key = None

    def __init__(self, request):
        settings = request.registry.settings
        # self._api_key = "key-d0f65daf04631f62bc5af6d0f6105b13"
        self._api_key = settings['mailgun.api_key']
        self._cta_url = settings['mailgun.cta_url']


    def send_email(self, from_addr, to_addrs, subject, body_text, body_html=None,
                   cc_addrs=None, bcc_addrs=None, reply_to_addr=None, attachments=None,
                   custom_vars=None, tags=None):


        message_data = {"from": from_addr,
                        "to": to_addrs,
                        "subject": subject,
                        "text": body_text}
        files = []

        if body_html:
            message_data['html'] = body_html

        if cc_addrs:
            assert isinstance(cc_addrs, (str, list))
            message_data['cc'] = cc_addrs

        if bcc_addrs:
            assert isinstance(bcc_addrs, (str, list))
            message_data['bcc'] = bcc_addrs

        if reply_to_addr:
            message_data['h:Reply-To'] = reply_to_addr

        if attachments:
            for file_path in attachments:
                files.append(("attachment", open(file_path)))

        if custom_vars:
            assert isinstance(custom_vars, dict)
            for key, value in custom_vars.items():
                message_data['v:{0}'.format(key)] = value

        if tags:
            assert isinstance(tags, list)
            message_data['o:tag'] = tags

        response = requests.post(
            "https://api.mailgun.net/v3/{}/messages".format(self._cta_url),
            auth=("api", self._api_key),
            data=message_data,
            files=files)

        if response.status_code != 200:
            pass
            
            
        return response
