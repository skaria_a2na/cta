import argparse
import sys
import os

import csv
import pandas as pd


from pyramid.paster import bootstrap, setup_logging
from sqlalchemy.exc import OperationalError

from .. import models


# Code to get the path
scriptDirectory = os.path.dirname(os.path.realpath("initialize_db.py"))
scriptDirectory = scriptDirectory + "/cambium_cta/scripts"


def fetch_table_data():
    global scriptDirectory

    # ============================================================ #
    # Code to load the csv data from current folder to dataframe
    # =========================================================== #
    df_country = pd.read_csv(scriptDirectory + "/country.csv")
    df_country = df_country[df_country.columns[[4,5]]]
    df_country = df_country.dropna()
    df_country = df_country.drop_duplicates()

    df_state = pd.read_csv(scriptDirectory + "/city.csv")
    df_state = df_state[df_state.columns[[4,7]]]
    df_state = df_state.dropna()
    df_state = df_state.drop_duplicates()
    return ([df_country,df_state])

def setup_models(dbsession):
    """
    Add or update models / fixtures in the database.

    """
    # Adding country and state table to db
    df_list = fetch_table_data()
    df_country = df_list[0]
    df_state = df_list[1]

    if not df_country.empty:
        for index, row in df_country.iterrows():
           model_country = models.country.Country(name=row['country_name'], code=row['country_iso_code'])
           dbsession.add(model_country)

    if not df_state.empty:
        for index, row in df_state.iterrows():
           model_state = models.state.State(state_name=row['subdivision_1_name'].encode('utf-8'), country_code=row['country_iso_code'])
           dbsession.add(model_state)


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'config_uri',
        help='Configuration file, e.g., development.ini',
    )
    return parser.parse_args(argv[1:])


def main(argv=sys.argv):
    args = parse_args(argv)
    setup_logging(args.config_uri)
    env = bootstrap(args.config_uri)

    try:
        with env['request'].tm:
            dbsession = env['request'].dbsession
            #print(dbsession)
            setup_models(dbsession)
    except OperationalError as e:
        print(e)
        print('''
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to initialize your database tables with `alembic`.
    Check your README.txt for description and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.
            ''')
