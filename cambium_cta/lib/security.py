import hashlib
from passlib.context import CryptContext

ITOA64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

pwdctx = CryptContext(
    schemes=['bcrypt'],
    )


def md5(s, raw=False):
    hash = hashlib.md5(s)
    if raw:
        return hash.digest()
    else:
        return hash.hexdigest()


def _hash_encode64(input, count, itoa64):
    output = ''
    i = 0
    while True:
        value = ord(input[i])
        i += 1
        output += itoa64[value & 0x3f]

        if i < count:
            value |= ord(input[i]) << 8

        output += itoa64[(value >> 6) & 0x3f]

        if i >= count:
            break
        i += 1

        if i < count:
            value |= ord(input[i]) << 16

        output += itoa64[(value >> 12) & 0x3f]

        if i > count:
            break
        i += 1

        output += itoa64[(value >> 18) & 0x3f]

        if i >= count:
            break

    return output


def _hash_crypt_private(password, setting, itoa64):
    output = '*'
    prefix = setting[:3]
    if prefix not in ('$H$', '$P$'):
        return output

    count_log2 = itoa64.find(setting[3])
    if count_log2 < 7 or count_log2 > 30:
        return output

    count = 1 << count_log2
    salt = setting[4:12]

    if len(salt) != 8:
        return output

    hash = md5(salt + password, True)
    while True:
        hash = md5(hash + password, True)
        count -= 1
        if not count:
            break

    output = setting[:12]
    output += _hash_encode64(hash, 16, itoa64)

    return output


def phpbb_check_hash(password, hash):
    if len(hash) == 34:
        return _hash_crypt_private(password, hash, ITOA64) == hash
    return md5(password) == hash


def encryptpassword(value):
    return pwdctx.encrypt(value)


def verifypassword(value, encrypted):
    if encrypted.startswith('$H$'):
        if isinstance(value, unicode):
            value = value.encode('utf8')
        return phpbb_check_hash(value, encrypted)
    return pwdctx.verify(value, encrypted)


