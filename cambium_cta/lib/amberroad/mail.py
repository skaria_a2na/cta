import logging

from pyramid.renderers import render

from pyramid_mailer.message import Message
from pyramid_mailer import get_mailer

log = logging.getLogger(__name__)

def sendaccessemail(request, user, action):
    message = getaccessmail(request, user, action)
    mailer = get_mailer(request)
    mailer.send(message)

def getaccessmail(request, user, action):
    assert action in ('granted', 'denied')
    template = ('cambiumsupport:templates/email/screening/access%s.txt'
                % action)
    body = render(template,
                  {'email': user.email},
                  request=request)
    message = Message(subject='Cambium Networks Access',
                      recipients=[user.email],
                      body=body)
    return message
