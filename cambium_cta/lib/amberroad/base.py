# # from cambium_cta.views.countries import getcountrycode
# import cambium_cta.models.User as db

# # EoD doesn't support the exact same set of country codes as us. This
# # dictionary maps our country codes onto the values that EoD
# # expects. For regions that don't exist in EoD at all (such as the
# # Canary Islands) we send an empty string.
# BADCOUNTRYCODES = {
#     # Canary Islands
#     'IC': '',
#     # Ceuta and Melilla
#     'EA': '',
#     # Kosovo
#     'XK': 'KV',
#     # Norfolk Island
#     'NF': '',
# }

# def getcountryid(country):
#     code = getcountrycode(country) or ''
#     return BADCOUNTRYCODES.get(code, code)

def getaddress(user):
    return user.structuredaddress or db.Address(address1=user.oldaddress,
                                                country=user.oldcountry)

    
