import logging
import re

# import cambium_cta.models.amberroad as eoddb
from .webservice import geteod
from . import mail

log = logging.getLogger(__name__)

def screeningstate(eodstatus):
    if eodstatus is None:
        return 'unscreened'
    # If the eodstatus record exists, it means we have at least
    # attempted to screen this user, so the only possible results now
    # are 'error', 'no-match', 'match', or 'potential-match'.
    if not eodstatus.ok:
        return 'error'
    matchstate = eodstatus.match
    results = {
        'No Match': 'no-match',
        'Match': 'match',
        'Potential Match': 'potential-match',
    }
    # For safety, we need to block users until we've received a
    # positive 'No Match' result. Unexpected match values will thus be
    # treated as potential matches.
    defaultresult = 'potential-match'
    return results.get(matchstate, defaultresult)

def isblockingstate(screeningstate):
    # Return True if the state in screeningstate should be considered
    # a "blocking" state
    if screeningstate in ('unscreened', 'no-match'):
        return False
    return True

def screenuser(request, user):
    assert user.uuid is not None
    dbsession = request.dbsession
    eod = geteod(request)
    try:
        response = eod.screenuser(user, request)
    except Exception as e:
        log.exception('Exception when screening user: %s', e)
        response = eoddb.EODStatus(
            statusdescription=str(e),
        )
    response.partnerid = user.uid
    response = dbsession.merge(response)
    return screeningstate(response)

def getstatechangeaction(oldstate, newstate):
    if oldstate == newstate:
        return None
    blockedstates = ['error', 'potential-match', 'match']
    if newstate == 'no-match':
        # Access is allowed. Were we previously blocked?
        if oldstate in blockedstates:
            return 'granted'
    elif newstate == 'match':
        # Access is permanently blocked. Were we previously pending?
        if oldstate in blockedstates:
            return 'denied'

def checkstatechange(request, user, oldeodstatus, neweodstatus):
    oldstate = screeningstate(oldeodstatus)
    newstate = screeningstate(neweodstatus)
    action = getstatechangeaction(oldstate, newstate)
    if action is not None:
        mail.sendaccessemail(request, user, action)

def shouldscreenonregistration(settings, email):
    # Screening on registration needs to be explicitly enabled in the config
    screeningenabled = settings.get('eod.screenonregistration', False)
    if not screeningenabled:
        return False

    # If screening is enabled, by default we screen every new
    # registration. The config key below can be set to limit screening
    # to emails matching a particular pattern.
    pattern = settings.get('eod.screenonregistrationregex', '')
    return bool(re.match(pattern, email))

def onregistrationcomplete(event):
    request = event.request
    settings = request.registry.settings
    user = event.user
    if shouldscreenonregistration(settings, user.email):
        screenuser(request, user)

def rescreenuser(request, user):
    oldstatus = user.eodstatus
    oldstate = screeningstate(oldstatus)
    newstate = screenuser(request, user)
    action = getstatechangeaction(oldstate, newstate)
    if action is not None:
        mail.sendaccessemail(request, user, action)
