import os
from xml.etree import ElementTree as ET
# from cStringIO import StringIO
import logging

from pyramid.settings import asbool
import requests
import io

from ..debughttp import VerboseSession

from . import base
from cambium_cta.models.user import User, UserProfile
from cambium_cta.models.amberroad import DeniedPartyScreeningStatus

log = logging.getLogger(__name__)

class ExportOnDemand(object):
    def __init__(self,
                 username,
                 password,
                 baseurl,
                 certpath,
                 orgcode,
                 companycode,
                 timeout,
                 verbose=False):
        if verbose:
            self._session = VerboseSession()
        else:
            self._session = requests.Session()
        self._session.auth = (username, password)
        if certpath:
            self._session.verify = certpath
        else:
            self._session.verify = True
        if not baseurl.endswith('/'):
            baseurl += '/'
        self.baseurl = baseurl
        self.orgcode = orgcode
        self.companycode = companycode
        self.timeout = timeout

    def getuserresponse(self, user, source):
        xml = getuserxml(user, self.orgcode, self.companycode, source)
        url = self.baseurl + 'SyncXMLIntegration'
        response = self._session.post(url, data=xml, timeout=self.timeout)
        return response

    def screenuser(self, user, source=None, request=None):
        response = None
        content = None
        try:
            response = self.getuserresponse(user, source)
            content = response.content
            return parseresponse(content, source)
        except Exception:
            log.exception(
                'EOD error for user %s\nresponse: %s\nbody: %s',
                user.email,
                response,
                content,
            )
            raise

class DummyExportOnDemand(object):
    attrs = ['match', 'embargohold', 'customembargomatch']
    def __init__(self, settings):
        for attr in self.attrs:
            if attr == 'match':
                default = 'No Match'
            else:
                default = 'N'
            default = settings.get('eod.dummyresult.%s' % attr, default)
            setattr(self, attr, default)

    def screenuser(self, user):
        result = EODStatus()
        result.partnerid = user.uid
        result.statuscode = 1000
        result.statusdescription = 'Success'
        for attr in self.attrs:
            setattr(result, attr, getattr(self, attr))
        return result

def parseresponse(responsexml, source="cta"):
    # ElementTree requires utf8-encoded byte strings
    # if isinstance(responsexml, unicode):
    #     responsexml = responsexml.encode('utf8')
    root = ET.fromstring(responsexml)
    assert root.tag == 'PartnerRecord'
    
    def gettext(key):
        elem = root.find(key)
        if elem is not None:
            return elem.text
    
    result = DeniedPartyScreeningStatus(
        partnerid=gettext('PartnerId'),
        match=gettext('Status'),
        embargohold=gettext('EmbargoHold'),
        customembargomatch=gettext('CustomEmbargoMatch'),
        statuscode=int(gettext('IntegrationStatus/StatusCode')),
        statusdescription=gettext('IntegrationStatus/StatusDescription'),
        from_application=source
    )
    if result.ok:
        # We have behaviour linked to the strings in the 'match'
        # field, so let's log any unexpected values.
        if result.match not in ('No Match', 'Potential Match', 'Match'):
            pass
    return result

def geteodfromsettings(settings):
    factory = settings.get('eod.factory', None)
    if factory is not None:
        return factory(settings)
    else:
        defaultcert = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                '..', '..',
                'certificates',
                'amberroad.crt',
            )
        )
        return ExportOnDemand(
            settings['eod.username'],
            settings['eod.password'],
            settings['eod.baseurl'],
            settings.get('eod.certpath', defaultcert),
            settings['eod.orgcode'],
            settings['eod.companycode'],
            int(settings.get('eod.timeout', 10)),
            asbool(settings.get('eod.debug', 'false')),
        )

def geteod(request):
    result = getattr(request, '__eod', None)
    if result is None:
        result = geteodfromsettings(request.registry.settings)
        request.__eod = result
    return result

def getuserfields(user, source="cta"):
    # AmberRoad provides a schema, PartnerXml.xsd, which describes the
    # supported fields. I'm only including the mandatory ones, the
    # ones that we currently have available, and some that we don't
    # have right now but might conceivably have at some point.

    if source in (None,'None'):
        source= "cta"

    user_name = str(user.first_name)+" "+ str(user.last_name)
    partner_id = str(user.uuid)

    user_profile_data = user.profile[0]

    basefields = [
        ('PartnerId', partner_id),
        ('Name', "test_company"),
        ('PrimaryContact', user_name),
        ('Address1', user_profile_data.street_address_1),
        ('Address2', user_profile_data.street_address_2),
        ('Address3', ""),
        ('Address4', ""),
        ('City', user_profile_data.town),
        ('State', user_profile_data.state),
        ('PostalCode', user_profile_data.postal_code),
        ('CountryCode', "IN"),
        ('Country', user_profile_data.country.name),
        ('Email', user.email),
    ]
    extensionfields = [
        ('Source', source),
        ('Support_ID', user.uuid),
    ]

    return basefields, extensionfields

def getuserxmlroot(user, orgcode, companycode, source):
    root = ET.Element('PartnerRecord', version="2")
    def add(name, value):
        e = ET.SubElement(root, name)
        e.text = value
        return e
    add('OrgCode', orgcode)
    add('CompanyCode', companycode)
    basefields, extensionfields = getuserfields(user, source)
    for field, value in basefields:
        add(field, value)

    ext = ET.SubElement(root, 'Extension',
                        extensionTemplate='PARTNER_HEADER')
    for field, value in extensionfields:
        e = ET.SubElement(ext, 'ExtensionField', name=field)
        e.text = value
    return root

def getuserxml(user, orgcode, companycode, source):
    root = getuserxmlroot(user, orgcode, companycode, source)
    tree = ET.ElementTree(root)
    buf = io.BytesIO()
    tree.write(buf, encoding='UTF-8', xml_declaration=True)
    return buf.getvalue()
    pass