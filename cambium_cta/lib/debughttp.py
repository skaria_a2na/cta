"""Tools for debugging HTTP interactions

This module provides a VerboseSession class that can be used in place
of requests.Session. All HTTP requests made with this class will be
logged, including headers and request bodies.
"""
import logging

import requests

class VerboseAdapter(object):
    def __init__(self, adapter, logname=__name__):
        self.adapter = adapter
        self.log = logging.getLogger(logname)

    def send(self, request, **kwargs):
        self.log.debug('> %s %s', request.method, request.url)
        for header in request.headers:
            self.log.debug('> %s: %s', header, request.headers[header])
        if request.body:
            self.log.debug('>\n> %s', request.body)
        else:
            self.log.debug('>\n> (body: %r)', request.body)
        response = self.adapter.send(request, **kwargs)
        self.log.debug('< Status: %s %s',
                       response.status_code,
                       response.reason)
        for header in response.headers:
            self.log.debug('< %s: %s', header, response.headers[header])
        if response.content:
            self.log.debug('<\n< %s', response.content)
        return response

class VerboseSession(requests.Session):
    def get_adapter(self, url):
        adapter = super(VerboseSession, self).get_adapter(url)
        return VerboseAdapter(adapter)
