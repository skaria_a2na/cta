/* All registration scripts goes here */
// cta_registration,company_registration, verify_email
$(document).ready(function(){

    // additional validation for jquery validate plugin - allow alphabets and space
    $.validator.addMethod("alphabetsnspace", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9 .-]*$/.test(value);
    });

    $.validator.addMethod("urlvalidator", function(value, element) {
       return this.optional(element) || /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$/.test(value);
    });

     $('#company_registration_form').validate({
         errorPlacement: function (error, element) {
             error.insertAfter(element);
         },
        errorClass:'error-form' ,
        validClass:'',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass)
        },
         rules: {
             company_name: {
                 required: true,
                 maxlength: 100
             },
             company_city: {
                 alphabetsnspace: true,
                 required: true,
                 maxlength: 80
             },
             company_zip: {
                 alphabetsnspace: true,
                 maxlength: 11,
                 required: true
             },
             company_state: {
                 alphabetsnspace: true,
                 required: true,
                 maxlength: 80
             },
             company_country: 'required',
             company_number: {
                 alphabetsnspace: true,
                 maxlength: 13,
                 required: true
             },
             company_address1: {
                 required: true,
                 maxlength: 250
             },
             company_website: {
                 urlvalidator: true,
                 required: true
             }
         },
         messages: {
             company_name: {
                 required: "Please enter company name"
             },
             company_city: {
                 required: "Please enter city.",
                 alphabetsnspace : "No special characters allowed"
             },
             company_zip: {
                 required: "Please enter zip/postal code.",
                 alphabetsnspace : "No special characters allowed"
             },
             company_state: {
                 required: "Please enter state",
                 alphabetsnspace : "No special characters allowed"
             },
             company_country: {
                 required: "Please enter country"
             },
             company_number: {
                 required: "Please enter company number",
                 alphabetsnspace : "No special characters allowed"
             },
             company_address1: {
                 required: "Please enter company address"
             },
             company_website: {
                 urlvalidator : "Please enter a valid url",
                 required: "Please enter your company website"
             },
         },
         submitHandler: function(form) {
            $("id_reg_com").attr("disabled",true);
            form.submit();
         }
     });

 if(company_exists == 1) {
     $('#comp-info').modal('show');
 }
 // code to get the list of country and state
 $('select[name="company_country"]').on("change",function(){
        value =$(this).val();
        $.ajax(
        {
            url : '/company_registration/get_state/',
            type : 'POST',
            data : {
                'country' : value
            },
            success: function(data) {
                company_dict = data.company_dict;
                country_list = data.country_list;
                state_list = data.state_list
                email = data.email
                selected_company_id = data.selected_company_id
            }
        }).done(function() {
          $('select[name="company_state"]').find('option').remove().end();
          $.each(state_list, function( index, value ) {
                $('select[name="company_state"]').append(new Option(value[1], value[0]));
            });
        });

    });

$('#comp_cancel').click(function(){
    window.location.href = '/registration/'+token.toString()
})

});
