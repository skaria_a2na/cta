/* All registration scripts goes here */
// cta_registration,company_registration, verify_email
$(document).ready(function(){

    //Verfify email submit function
    $("#verify_email_form").submit(function () {
        // Get the Login Name value and trim it
        var bus_email = $.trim($('#id_bus_email').val());
        // Check if empty of not
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        if (bus_email === '' || expr.test(email)) {
            $("#id_bus_email").addClass( "error-form" );
            $("#id_bus_error").text("Please enter a valid business email address.")
            return false;
        }
        else
        {
            $(this).submit()
        }
    });

    $('#comp_cancel').click(function(){
        window.location.href = '/registration/'+token.toString()
    })

});
