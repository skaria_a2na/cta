/* All registration scripts goes here */
// cta_registration,company_registration, verify_email
$(document).ready(function(){

    // code to load the local storage data
    if (sessionStorage.getItem("var_fname") != 'false') {
        $('#id_first_name').val(sessionStorage.getItem("var_fname"));
    }
    if (sessionStorage.getItem("var_lname") != 'false') {
        $('#id_last_name').val(sessionStorage.getItem("var_lname"));
    }
    if (sessionStorage.getItem("var_contact") != 'false') {
        $("#id_contact_number").val(sessionStorage.getItem("var_contact"));
    }
//    if (sessionStorage.getItem("var_email") != 'false') {
//        $("#id_bus_email").val(sessionStorage.getItem("var_email"));
//    }
    if (sessionStorage.getItem("var_stadd1") != 'false') {
        $("#id_address_1").val(sessionStorage.getItem("var_stadd1"));
    }
    if (sessionStorage.getItem("var_stadd2") != 'false') {
        $("#id_address_2").val(sessionStorage.getItem("var_stadd2"));
    }
    if (sessionStorage.getItem("var_town") != 'false') {
        $("#id_town").val(sessionStorage.getItem("var_town"));
    }
    if (sessionStorage.getItem("var_postalcode") != 'false') {
        $("#id_postal_code").val(sessionStorage.getItem("var_postalcode"));
    }
    if (sessionStorage.getItem("var_state") != 'false') {
        $("#id_state").val(sessionStorage.getItem("var_state"));
    }
    if (sessionStorage.getItem("var_country") != 'null') {
        $("#id_country").val(sessionStorage.getItem("var_country"));
    }
    if (sessionStorage.getItem("company_mail") != 'false' || sessionStorage.getItem("company_mail") != 'null' ) {
        $("#id_company_mail").val(sessionStorage.getItem("company_mail"));
    }
    if (sessionStorage.getItem("var_password") != 'false') {
        $('#id_pwd').val(sessionStorage.getItem("var_password"))
    }
    if (sessionStorage.getItem("var_cnfm_password") != 'false') {
        $('#id_cnfrm_pwd').val(sessionStorage.getItem("var_cnfm_password"))
    }
    if (sessionStorage.getItem("var_sms") == 'true'){
        $('#sms_checkbox').attr('checked', true);
    }
    if (sessionStorage.getItem("var_email") == 'true'){
        $('#email_checkbox').attr('checked', true);
    }
    if (sessionStorage.getItem("var_call") == 'true'){
        $('#call_checkbox').attr('checked', true);
    }
    if (sessionStorage.getItem("var_postal") == 'true'){
        $('#postal_checkbox').attr('checked', true);
    }
    if (sessionStorage.getItem("var_privacy") == 'true'){
        $('#privacy_checkbox').attr('checked', true);
    }

      // Store to local data storage
    $("#id_reg_com").on('click', function(){
        if (typeof(Storage) !== "undefined") {
            fname = $("#id_first_name").val()
            lname = $("#id_last_name").val()
            contact = $("#id_contact_number").val()
            email = $("#id_bus_email").val()
            stadd1 = $("#id_address_1").val()
            stadd2 = $("#id_address_2").val()
            town = $("#id_town").val()
            postalcode = $("#id_postal_code").val()
            state = $("#id_state").val()
            country = $("#id_country").val()
            company_mail = $("#id_company_mail").val()
            password = $("#id_pwd").val()
            cnfm_password = $("#id_cnfrm_pwd").val()
            sms = $('#sms_checkbox').is(':checked')
            email = $('#email_checkbox').is(':checked')
            call = $('#call_checkbox').is(':checked')
            postal = $('#postal_checkbox').is(':checked')
            privacy = $('#privacy_checkbox').is(':checked')
            sessionStorage.var_fname=fname;
            sessionStorage.var_lname=lname;
            sessionStorage.var_contact = contact;
            sessionStorage.var_email = email;
            sessionStorage.var_stadd1 = stadd1;
            sessionStorage.var_stadd2 = stadd2;
            sessionStorage.var_town = town;
            sessionStorage.var_postalcode = postalcode;
            sessionStorage.var_state = state;
            sessionStorage.var_country = country;
            sessionStorage.company_mail = company_mail;
            sessionStorage.var_password = password
            sessionStorage.var_cnfm_password = cnfm_password
            sessionStorage.var_sms = sms
            sessionStorage.var_email = email
            sessionStorage.var_call = call
            sessionStorage.var_postal = postal
            sessionStorage.var_privacy = privacy
        }
    });

    // additional validation for jquery validate plugin - allow alphabets and space
    $.validator.addMethod("alphabetsnspace", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9 .-]*$/.test(value);
    });

    //^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$
    $.validator.addMethod("passwordvalidator", function(value, element) {
        return this.optional(element) || /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d\D]{8,}$/.test(value);
    });

    $.validator.addMethod("incNoEmail", function(value, element) {
        var email = $('[name=bus_email]').val();
        return value.indexOf(email) == -1

    });

    $.validator.addMethod("urlvalidator", function(value, element) {
        return this.optional(element) || /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$/.test(value);
    });

    $('#registration_form').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        errorClass:'error-form' ,
        validClass:'',
          highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass);
          },
          unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass)
          },
        rules: {
            firstname: {
                alphabetsnspace: true,
                required: true,
                maxlength: 50
            },
            lastname: {
                alphabetsnspace: true,
                required: true,
                maxlength: 50
            },
            password: {
                required: true,
                passwordvalidator:true,
                incNoEmail:true,
                minlength: 8
            },
            confirm_pass: {
                required: true,
                minlength: 8,
                equalTo: '[name="password"]'
            },
            contact_num: {
                 alphabetsnspace: true,
                 maxlength: 13
             },
            addr_1: {
                required: true,
                 maxlength: 250
                },
            city: {
                alphabetsnspace: true,
                required: true,
                maxlength: 80
            },
            postal_code: {
                alphabetsnspace: true,
                maxlength: 11,
                required: true
            },
            state: {
                alphabetsnspace: true,
                required: true,
                maxlength: 80
            },
            company_name: {
                required: true,
            },
            country: 'required'
        },
        messages: {
            firstname: {
                required: "Please enter your firstname",
                alphabetsnspace : "No special characters allowed",
            },
            lastname: {
                required :"Please enter your lastname",
                alphabetsnspace : "No special characters allowed"
            },
            password: {
                required: "Please provide a password",
                passwordvalidator: "Please enter atleast one digit and one character",
                incNoEmail: "Cannot include username in your password."
            },
            confirm_pass: {
                required: "Please provide confirm password",
                equalTo : "Please re-enter the same password",
                minlength: "Please re-enter the same password"
            },
            contact_num:{
                alphabetsnspace : "No special characters allowed",
                maxlength : "Maximum 13 digits allowed."
            },
            city: {
                required: "Please enter city",
                alphabetsnspace : "No special characters allowed"
            },
            postal_code: {
                required: "Please enter zip",
                alphabetsnspace : "No special characters allowed"
            },
            state: {
                required: "Please enter state",
                alphabetsnspace : "No special characters allowed"
            },
            company_name: {
                required: "Please select company",
            },
            email: "Please enter a valid email address",
            country: "Please enter your country",
            addr_1: "Please enter address"
        },
        submitHandler: function(form) {

            var email = $('[name=bus_email]').val();
            var password = $('[name=password]').val();

            if ( $('#privacy_checkbox').is(':checked') != true){
                $('#id_privacy_msg').show();
                $('#id_privacy_msg').text("You must accept the terms&conditions to continue!");
            }
            else{
                form.submit();
            }
        }
    });

    // code to select the country and states corresponding to country selected.

    $('select[name="country"]').on("change",function(){
        value =$(this).val();
        $.ajax(
        {
            url : '/registration/get_state/',
            type : 'POST',
            data : {
                'country' : value
            },
            success: function(data) {
                company_dict = data.company_dict;
                country_list = data.country_list;
                state_list = data.state_list
                email = data.email
                token = data.token
                selected_company_id = data.selected_company_id
            }
        }).done(function() {
            $('select[name="state"]').find('option').remove().end();
            $.each(state_list, function( index, value ) {
              $('select[name="state"]').append(new Option(value[1], value[0]));
            });

        });

    });


    $('#comp_cancel').click(function(){
        window.location.href = '/registration/'+token.toString()
    })

});
