/* Contains the validation for the login page */

$(document).ready(function(){

//    Validation to check whether the fields are empty
    $('#id_login_form').submit(function () {

        // Get the Login Name value and trim it
        var id_email = $.trim($('#id_email').val());
        var id_pwd = $.trim($('#id_pwd').val());

        // Check if empty of not
        if (id_email === '' || id_pwd === '') {
            $('#login_error_div').show()
            return false;
        } else {
                $("#id_login_form").submit();
            }
       });

});

