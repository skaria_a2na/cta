/* Contains the validation for the forgot password page */

$(document).ready(function(){

    //Validation to check whether the fields are empty
    $('#forgot_password_form').submit(function () {

        // Get the Login Name value and trim it
        var id_email = $.trim($('#id_reset_email').val());


        // Check if empty of not
        if (id_email === '') {
        $('#reset_error_div').show();
        $('#reset_success_div').hide();
        return false;
    	}
        else{
            $('#reset_error_div').hide();
            $('#reset_success_div').show();
            return true;
        }

    event.preventDefault();
    });
    $('#reset_password_form').submit(function(){
        // Get password and confirm password fields from reset password page
        var pwd = $('#id_reset_pwd').val();
        var con_pwd = $('#id_reset_confirm_pwd').val();
        if(pwd === con_pwd)
        {
            //pwd matches successfully
            $('#reset_success_pwd').show();
            $('#reset_failure_pwd').hide();
            return true;
        }
        else{
            //password mismatch
            $('#reset_success_pwd').hide();
            $('#reset_failure_pwd').show();
            return false;
        }

    });
});
