
/* Contains the validation for the profile page */

$(document).ready(function(){

    $('[name=current_password]').click(function () {
        $('#password_error_div').hide()
        $('#pass_change_success').text('')
    });
    $('[name=new_password]').click(function () {
        $('#password_error_div').hide()
        $('#pass_change_success').text('')
    });
    $('[name=confirm_password]').click(function () {
        $('#password_error_div').hide()
        $('#pass_change_success').text('')
    });

    $.validator.addMethod("passwordvalidator", function(value, element) {
        return this.optional(element) || /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d\D]{8,}$/.test(value);
    });

    // additional validation for jquery validate plugin - allow alphabets and space
    $.validator.addMethod("alphabetsnspace", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9 .-]*$/.test(value);
    });

    $('#id_profile_update_form').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        errorClass:'error-form' ,
        validClass:'',
          highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass);
          },
          unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass)
          },
        rules: {
            firstname: {
                alphabetsnspace: true,
                required: true,
                maxlength: 50
            },
            lastname: {
                alphabetsnspace: true,
                required: true,
                maxlength: 50
            },
            contact_num: {
                 alphabetsnspace: true,
                 maxlength: 13
             },
            addr_1: {
                required: true,
                 maxlength: 200
                },
            city: {
                alphabetsnspace: true,
                required: true,
                maxlength: 80
            },
            postal_code: {
                alphabetsnspace: true,
                maxlength: 11,
                required: true
            },
            state: {
                alphabetsnspace: true,
                required: true,
                maxlength: 80
            },
            country: 'required'
        },
        messages: {
            firstname: {
                required: "Please enter your firstname",
                alphabetsnspace : "No special characters allowed",
            },
            lastname: {
                required :"Please enter your lastname",
                alphabetsnspace : "No special characters allowed"
            },
            contact_num:{
                alphabetsnspace : "No special characters allowed",
                maxlength : "Maximum 13 digits allowed."
            },
            city: {
                required: "Please enter city",
                alphabetsnspace : "No special characters allowed"
            },
            postal_code: {
                required: "Please enter zip",
                alphabetsnspace : "No special characters allowed"
            },
            state: {
                required: "Please enter state",
                alphabetsnspace : "No special characters allowed"
            },
            country: "Please enter your country",
            addr_1: "Please enter address"
        },
        submitHandler: function(form) {
            var firstname = $.trim($('[name=firstname]').val());
            var lastname = $.trim($('[name=lastname]').val());
            var contact_num = $.trim($('[name=contact_num]').val());
            var addr_1 = $.trim($('[name=addr_1]').val());
            var addr_2 = $.trim($('[name=addr_2]').val());
            var city = $.trim($('[name=city]').val());
            var state = $.trim($('[name=state]').val());
            var postal_code = $.trim($('[name=postal_code]').val());
            var country = $.trim($('[name=country]').val());

            $.ajax({
                url     : 'edit_profile',
                type    : 'post',
                data: JSON.stringify({
                    'firstname': firstname,
                    'lastname': lastname,
                    'contact_num': contact_num,
                    'addr_1': addr_1,
                    'addr_2': addr_2,
                    'city': city,
                    'state': state,
                    'postal_code': postal_code,
                    'country': country
                }),
                dataType: 'json',
                success : function(response) {
                    alert("this is a success alert")
                        if(response.success){
                            $('#successcontact-modal').modal('show')
                        }else{
                            $('#profile_update_error_div').show()
                            $('#profile_update_div_text').val(response.message)
                        }
                },
                error   : function(data){
                    console.log|(data.errors);
                }
            });
        }
    });

    $('#id_change_password_form').validate({
        errorClass:'error-form' ,
        validClass:'',
          highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass);
          },
          unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass)
          },
        rules: {

            current_password: {
                required: true
            },
            new_password: {
                required: true,
                passwordvalidator:true,
                minlength: 8
            },
            confirm_password: {
                required: true,
                minlength: 8,
                equalTo: '[name="new_password"]'
            }
        },
        messages: {
            current_password: {
                required: "Please enter your old password.",
            },
            new_password: {
                required: "Please provide a password.",
                passwordvalidator: "Please enter atleast one digit and one character.",
            },
            confirm_password: {
                required: "Please provide confirm password.",
                equalTo : "Please re-enter the same password.",
                minlength: "Please re-enter the same password."
            }
        },
        submitHandler: function(form) {
            var current_password = $('[name=current_password]').val();
            var new_password = $('[name=new_password]').val();
            var confirm_password = $('[name=confirm_password]').val();

            $.ajax({
                url     : 'change_password',
                type    : 'post',
                data: JSON.stringify({
                    'current_password': current_password,
                    'new_password': new_password,
                    'confirm_password': confirm_password
                }),
                dataType: 'json',
                success : function(response) {
                        if(response.success){
                            $('#pass_change_success').text(response.message)
                        }else{
                            $('#password_error_div').show()
                            $('#password_error_div_text').val(response.message)
                        }
                },
                error   : function(data){
                    console.log|(data.errors);
                }
            });
        }
    });

    $('#id_save_preferences').on('click',function(){
	    if($('#id_sms_checkbox').prop("checked") == true){
	        sms = 'True'
	    }
	    else
	    {
	    	sms='False'
	    }

	    if($('#id_email_checkbox').prop("checked") == true){
	        email = 'True'
	    }
	    else
	    {
	    	email='False'
	    }
	    if($('#id_call_checkbox').prop("checked") == true){
	        call = 'True'
	    }
	    else
	    {
	    	call='False'
	    }
	    if($('#id_postal_checkbox').prop("checked") == true){
	        postal = 'True'
	    }
	    else
	    {
	    	postal='False'
	    }
        alert("changing communication preferences")
	    $.ajax(
        {
            url : 'communication_preferences',
            data : {
            	'sms' : sms,
            	'email':email,
            	'call':call,
            	'postal':postal
        			},
            type : 'POST'
        }).done
        (function() {
            alert("toggle fn");
            $('#communication-pref').modal('hide');
        });
        $('#id_save_preferences').attr('disabled','true');
	});

    $(".communication-tag").click(function(){
        $('#profile-modal').modal('toggle');
        $.ajax(
        {
            url : 'load_communication_preferences',
            type : 'POST'
        }).done(function(data) {
            sms = data.sms;
            email = data.email;
            call = data.call;
            postal = data.postal;
            if(sms =="true"){
                $('#id_sms_checkbox').attr('checked','checked');
            }
            else{
                $('#id_sms_Checkbox').attr('checked','');
            }

            if(email == "true"){
                $('#id_email_checkbox').attr('checked','checked');
            }
            else{
                $('#id_email_Checkbox').attr('checked','');
            }

            if(call =="true"){
                $('#id_call_checkbox').attr('checked','checked');
            }
            else{
                $('#id_call_Checkbox').attr('checked','');
            }

            if(postal == "true"){
                $('#id_postal_checkbox').attr('checked','checked');
            }
            else{
                $('#id_postal_Checkbox').attr('checked','');
            }
        });

    });


    $('#prf-edit-modal').on('hidden.bs.modal', function () {
        location.reload()
    })

});
