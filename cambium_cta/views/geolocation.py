from cambium_cta.models.country import Country
from cambium_cta.models.state import State


class Geolocation:

    def __init__(self, request):
        self.request = request
        self.country = ""
        self.state = ""

    def get_country(self):
        # ==============================#
        # Retrive country list
        # ============================== #
        country_data = self.request.dbsession.query(Country).all()
        country_list = []
        for each_country in country_data:
        	country_list.append([each_country.code,each_country.name])
        return country_list

    def get_state(self,country):
    	# ==============================#
        # Retrive state list
        # ============================== #
        state_data = self.request.dbsession.query(State).filter_by(country_code = country).all()
        state_list = []
        for each_state in state_data:
            state_list.append([each_state.id, each_state.state_name])
        return state_list
