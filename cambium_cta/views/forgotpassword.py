import uuid
import datetime

from sqlalchemy.sql.expression import exists

from cambium_cta.common.mailgun_client import MailgunClient

from cambium_cta.models.company import Company
from cambium_cta.models.user import User
from cambium_cta.models.user import UserProfile
from cambium_cta.models.userinvitation import UserInvites
from cambium_cta.models.userinvitation import PasswordResetInvites

from pyramid.renderers import render


class ForgotPassword:

    def __init__(self, request):
        self.request = request
        self.buz_mail = ""

    def validate_business_email(self):
        # ==============================#
        # email validation in database
        # ==============================#
        self.buz_email = self.request.POST.get('reset_email')
        user_exist = self.request.dbsession.query(
            User).filter_by(email=self.buz_email).first()
        if (user_exist):
            # code to check if the user reset link is expired
            return True
        else:
            return False

    def process_business_email(self):
        bus_email = self.request.POST.get('reset_email')
        print(bus_email)

        user_exists = self.request.dbsession.query(exists().where(User.email == self.buz_email)).scalar()
        if user_exists:
            # show message user already exists
            invite_details = self.request.dbsession.query(PasswordResetInvites).filter_by(email=bus_email).first()
            if invite_details:
                print("check expiration date")
                if invite_details.expiry_date > datetime.datetime.utcnow():
                    print("not expired")
                    # Invite not expired then send link again
                    self.send_user_registration_link(invite_details)
                else:
                    # delete existing invite and send a new one
                    self.request.dbsession.delete(invite_details)
                    self.request.dbsession.flush()
                    invitation = self.create_password_reset_invitation(bus_email)
                    self.send_user_registration_link(invitation)
            else:
                #send a new invite link
                invitation = self.create_password_reset_invitation(bus_email)
                self.send_user_registration_link(invitation)

            return True
        return False

    def send_user_registration_link(self, invitation):
        bus_email = self.request.POST.get('reset_email')
        settings = self.request.registry.settings

        # ======================================= #
        # save user invitatation data
        # ======================================= #

        to_address = bus_email
        from_address = settings['mailgun.from_email']
        reg_uuid = invitation.uuid
        url = self.request.route_url('reset_password', token=reg_uuid)

        # ====================================== #
        # Creating email template body
        # ====================================== #
        body = render('cambium_cta:templates/email/reset_password.txt',
                      {'email': to_address, 'url': url},
                      request=self.request)

        # ===================================== #
        # Sending the mail to the user
        # ===================================== #
        mail_client = MailgunClient(self.request)
        mail_client.send_email(from_addr=from_address,
                               to_addrs=to_address,
                               subject="Cambium Total Access Password Reset",
                               body_text=body)
        return mail_client

    def create_password_reset_invitation(self, bus_email, came_from="cta"):
        invitation_object = PasswordResetInvites(
            email=bus_email,
            came_from=came_from)
        self.request.dbsession.add(invitation_object)
        self.request.dbsession.flush()
        return invitation_object

    def get_user_invitation_by_uuid(self, reg_uuid):
        return self.request.dbsession.query(PasswordResetInvites).filter_by(uuid=reg_uuid).first()

    def invitation_valid(self, invitation):
        if invitation.expiry_date > datetime.datetime.utcnow():
            if invitation.is_active:
                return True
        return False
