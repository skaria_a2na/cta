#Package level imports
from pyramid.security import (forget, )
from pyramid.view import (view_config, )
from pyramid.security import (remember, )
from pyramid.httpexceptions import (HTTPForbidden, HTTPFound, )

#Controller level imports
from .geolocation import Geolocation
from .registration import Registration
from .authentication import Authentication
from .forgotpassword import ForgotPassword

#Model Level imports
from cambium_cta.models.company import Company
from cambium_cta.models.country import Country
from cambium_cta.models.user import User, UserProfile
from cambium_cta.models.amberroad import DeniedPartyScreeningStatus

#Common library based imports
from cambium_cta.lib.amberroad.policy import screeningstate, isblockingstate

#global variables
global country_data
global company_dict

@view_config(route_name='login', renderer='login.jinja2')
def login(request):
    if request.user:
        return HTTPFound(request.route_url('home', uuid=request.user.uuid))
    if request.method == 'POST':
        auth = Authentication(request)
        login_status = auth.handle_login()
        if login_status:
            return login_status
        else:
            return {'login_sucess': False}

    #Default should be True       
    return {'login_sucess': True}


@view_config(route_name='forgot_password', renderer='forgot_password.jinja2')
def forgot_password(request):

    if request.method == 'POST':
        reg = ForgotPassword(request)
        if reg.validate_business_email():
            # code to send mail
            send_invite = reg.process_business_email()
            return {'is_success': True}
        else:
            return {'is_error': True}
    return {}


@view_config(route_name = 'reset_password', renderer = 'reset_password.jinja2')
def reset_password(request):
    reg_uuid = request.url.split('/')[-1]

    reg = ForgotPassword(request)
    invitation = reg.get_user_invitation_by_uuid(reg_uuid)

    if not invitation:
        return HTTPFound(request.route_url('reset_link_expired'))

    if not reg.invitation_valid(invitation):
        return HTTPFound(request.route_url('reset_link_expired'))

    email = invitation.email
    if request.method == "POST":
        #  code to update the password here
        user = request.dbsession.query(User).filter_by(email=email).first()
        user.password = request.params['reset_pwd']
        request.dbsession.flush()
        return HTTPFound(request.route_url('login'))

    return {"email": email, "token": reg_uuid}


@view_config(route_name='logout')
def logout(request):
    headers = forget(request)
    next_url = request.route_url('login')
    return HTTPFound(location=next_url, headers=headers)


@view_config(route_name='profile_view', renderer='profile.jinja2')
def profile(request):

    user_profile = request.dbsession.query(UserProfile).filter_by(
        user_id=request.user.id).first()

    user_company = request.dbsession.query(Company).filter_by(
        id=request.user.company_id).first()

    country_code = request.dbsession.query(Country).filter_by(
        id=user_profile.country_id).first().code

    data = Geolocation(request)
    country_list = data.get_country()
    state_list = data.get_state(country_code)

    return {'login_sucess': True,
        'user_profile': user_profile,
        'user_company': user_company,
        'state_list' : state_list,
        'country_list': country_list,
        'token':request.user.uuid
        }


@view_config(route_name='verify_email', renderer='verify_email.jinja2')
def verify_email(request):
    if request.method == 'POST':
        reg = Registration(request)
        if reg.validate_business_email():
            send_invite = reg.process_business_email()
            return {'is_success': True, 'is_error': False}
        else:
            return {'is_success': False, 'is_error': True}
    return {}


@view_config(route_name='cta_registration',
             renderer='cta_registration.jinja2')
def cta_registration(request):
    global country_data
    global company_dict
    
    # code to retrive all country data
    data = Geolocation(request)
    country_data = data.get_country()
    selected_company_id = ""
    # Extracting the registration id from url
    reg_uuid = request.url.split('/')[-1]
    print("reg_uuid",reg_uuid)

    reg = Registration(request)
    invitation = reg.get_user_invitation_by_uuid(reg_uuid)

    if not invitation:
        return HTTPFound(request.route_url('link-expired'))

    if not reg.invitation_valid(invitation):
        return HTTPFound(request.route_url('link-expired'))

    email = invitation.email
    request.session['email'] = email

    country_list = request.dbsession.query(Country).all()
    # ============================================ #
    # Logic for getting the company based in email
    # domain
    # ============================================ #
    email_domain = str(email).split('@')[-1]
    query = request.dbsession.query(Company)
    records = query.filter_by(email_domain=email_domain.strip()).all()
    company_dict = {}

    for record in records:
        company_dict[str(record.id)] = record.name + ', ' + record.town

    # company_id will be available only after company data insertion
    if "user_company_id" in request.session:
        selected_company_id = request.session["user_company_id"]

    if request.method == "POST":
        user_object = reg.register_user()
        if user_object is not None:
            next_url = request.route_url('home', uuid=user_object.uuid)
            headers = remember(request, user_object.id)
            return HTTPFound(next_url, headers=headers)
        return HTTPFound(request.route_url('login'))

    request.session['token'] = reg_uuid
    request.session['selected_company_id'] = selected_company_id

    return {"country_list": country_data,
            "state_list":[],
            'company_dict': company_dict,
            "email": email, "token": reg_uuid,
            "selected_company_id": selected_company_id}


@view_config(route_name='company_registration',
             renderer='company_registration.jinja2')
def company_registration(request):
    global country_data

    email = ""
    # Extracting the registration id from url
    reg_uuid = request.url.split('/')[-1]
    if "email" in request.session:
        email = request.session["email"]

    if request.method == 'POST':
        reg = Registration(request)
        result = reg.register_company()
        if isinstance(result, dict) and result["server_error"]:
            return {"token": reg_uuid, "email": email, "server_error": True,
                    "company_exists": 0}
        elif isinstance(result, dict) and result["company_exists"]:
            return {"token": reg_uuid, "email": email, "server_error": False,
                    "company_exists": 1}
        else:
            return HTTPFound(
                request.route_url('cta_registration', token=reg_uuid))

    return {"token": reg_uuid, "email": email, "company_exists": 0,
            "country_list": country_data,"state_list": []}

@view_config(route_name='home',renderer='home.jinja2')
def home(request):
    user=request.user
    if user is None:
        raise HTTPForbidden

    if request.method == 'GET':
        amberroad_data = request.dbsession.query(DeniedPartyScreeningStatus).filter_by(
        partnerid=request.user.uuid).first()

        state = screeningstate(amberroad_data)
        if isblockingstate(state):
            return {"blocked":True}
        return {}


@view_config(route_name='reset_link_expired', renderer='password_verification_link_expiry.jinja2')
@view_config(route_name='link-expired',
             renderer='email-verify-link-expired.jinja2')
def link_expired(request):
    return {}

@view_config(route_name='get_company_state',renderer = "json")
@view_config(route_name='get_state',renderer = "json")
def get_state(request):
    global country_data
    global company_dict

    reg_uuid = request.session['token']
    selected_company_id = request.session['selected_company_id']
    email = request.session['email']
    token = request.session['token']

    # code goes here for retriving state data
    data = Geolocation(request)
    country = request.params['country']
    state_data = data.get_state(country)
    return {"country_list": country_data,
        "state_list":state_data,
        'company_dict': company_dict,
        "email": email, "token": reg_uuid,
        "selected_company_id": selected_company_id,
        "company_exists": 0,}



