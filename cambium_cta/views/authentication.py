from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
)

from cambium_cta.models.user import User
from pyramid.security import unauthenticated_userid


class Authentication:

    def __init__(self, request):
        self.request = request

    def handle_login(self):
        login_email = self.request.params['login_email']
        password = self.request.params['password']
        user = self.request.dbsession.query(User).filter_by(
            email=login_email).first()
        if user is not None and user.verify_password(password):
            next_url = self.request.route_url('home', uuid=user.uuid)
            headers = remember(self.request, user.id)
            return HTTPFound(next_url, headers=headers)

        return False


def get_user(request):
    # the below line is just an example, use your own method of
    # accessing a database connection here (this could even be another
    # request property such as request.db, implemented using this same
    # pattern).
    dbconn = request.registry.settings['dbconn']
    userid = unauthenticated_userid(request)
    if userid is not None:
        # this should return None if the user doesn't exist
        # in the database
        return dbconn['users'].query({'id':userid})

