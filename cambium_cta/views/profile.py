import ast
import json

from pyramid.response import Response
from pyramid.view import (view_config, )

from cambium_cta.models.user import UserProfile
from cambium_cta.models.company import Company
from cambium_cta.models.country import Country

from cambium_cta.views.geolocation import Geolocation

@view_config(route_name='profile.communication_preferences', renderer='profile.jinja2')
def communication_preferences(request):
    # code to get user profile and change communication preferences
    user_profile = request.dbsession.query(UserProfile).filter_by(
        user_id=request.user.id).first()

    user_company = request.dbsession.query(Company).filter_by(
        id=request.user.company_id).first()

    if request.method == 'POST':
        form_values = request.params
        sms =  form_values.get('sms')
        email =  form_values.get('email')
        call =  form_values.get('call')
        postal =  form_values.get('postal')
        user_profile.comm_preferences=json.dumps({
                'sms': sms,
                'email': email,
                'call': call,
                'postal': postal
            })
        request.dbsession.flush()

    return {'login_sucess': True,
        'user_profile': user_profile,
        'user_company': user_company
        }


@view_config(route_name='profile.load_communication_preferences',renderer='json')
def load_communication_preferences(request):
    user_profile = request.dbsession.query(UserProfile).filter_by(
        user_id=request.user.id).first()

    user_company = request.dbsession.query(Company).filter_by(
        id=request.user.company_id).first()
    if request.method=="POST":
        # retrive the communication preferences from db
        comm_data = ast.literal_eval(request.dbsession.query(UserProfile).filter_by(user_id=request.user.id).first().comm_preferences)

        return{'sms':comm_data['sms'].lower(),
            'email': comm_data['email'].lower(),
            'postal': comm_data['postal'].lower(),
            'call': comm_data['call'].lower()}


@view_config(route_name='profile.change_password')
def change_password(request):
    current_password = request.json_body.get('current_password')
    new_password = request.json_body.get('new_password')
    confirm_password = request.json_body.get('confirm_password')

    current_user = request.user
    success = False
    if not current_user.verify_password(current_password):
        message = 'paswword dont match records.'
    elif new_password != confirm_password:
        message = 'passwords not same.'
    else:
        try:
            current_user.password = new_password
            request.dbsession.flush()
            message = 'Change password successful.'
            success = True
        except Exception as e:
            message = 'Error: Cannot update password.'
        # request.session.flash(message)
    return Response(body=json.dumps({
        'success': success,
        'message': message
    }), content_type='text/plain')


@view_config(route_name='profile.edit_profile')
def edit_profile(request):
    success = False
    current_user = request.user

    firstname = request.json_body.get('firstname')
    lastname = request.json_body.get('lastname')
    contact_num = request.json_body.get('contact_num')
    addr_1 = request.json_body.get('addr_1')
    addr_2 = request.json_body.get('addr_2')
    city = request.json_body.get('city')
    state_id = request.json_body.get('state')
    postal_code = request.json_body.get('postal_code')
    country = request.json_body.get('country')

    profile = request.dbsession.query(UserProfile).filter_by(
        user_id=request.user.id).first()
    country_id = request.dbsession.query(Country).filter_by(code=country).first().id

    try:
        current_user.first_name = firstname
        current_user.last_name = lastname
        request.dbsession.flush()

        profile.phone_number = contact_num
        profile.street_address_1 = addr_1
        profile.street_address_2 = addr_2
        profile.town = city
        profile.state_id = int(state_id)
        profile.postal_code = postal_code
        profile.country_id = int(country_id)
        request.dbsession.flush()

        message = 'Profile update successful.'
        success = True
    except Exception as e:
        message = 'Error: Cannot update profile.'

    return Response(body=json.dumps({
        'success': success,
        'message': message
    }), content_type='text/plain')
