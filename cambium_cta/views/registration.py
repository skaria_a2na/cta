import datetime

from pyramid.renderers import render
from sqlalchemy.sql.expression import exists

from zerobounce import ZeroBounceAPI

from cambium_cta.common.mailgun_client import MailgunClient
from cambium_cta.lib.amberroad.webservice import geteod

from cambium_cta.models.state import State
from cambium_cta.models.company import Company
from cambium_cta.models.country import Country
from cambium_cta.models.userinvitation import UserInvites
from cambium_cta.models.user import User, UserProfile, UserActivityLogger


class Registration:

    def __init__(self, request):
        self.request = request
        self.buz_mail = ""

    def validate_business_email(self):
        # ==============================#
        # Zerobounce email validation
        # ==============================#
        settings = self.request.registry.settings
        bus_email = self.request.POST.get('bus_email')

        zba = ZeroBounceAPI(settings['zerobounce.api_key'])
        bus_email_status = zba.validate(bus_email)
        self.buz_mail = bus_email

        if 'status' in bus_email_status:
            if bus_email_status['status'].lower() == 'valid':
                if 'free_email' in bus_email_status:
                    if bus_email_status['free_email']:
                        return False
                return True
            elif bus_email_status['status'].lower() in ['spamtrap', 'invalid']:
                return False
            elif (bus_email_status['status'].lower() in ['donotmail'] and
                  bus_email_status['sub_status']
                  in ['role_based', 'role_based_catch_all']):
                return True
            else:
                return False
        else:
            return False

    def process_business_email(self):
        bus_email = self.request.POST.get('bus_email')

        user_exists = self.request.dbsession.query(
            exists().where(User.email == bus_email)).scalar()

        if user_exists:
            # =================================== #
            # show message user already exists
            # =================================== #
            self.send_fake_registration_mail()
            return True
        else:  # user not exists
            invite_details = self.request.dbsession.query(
                UserInvites).filter_by(email=bus_email).first()
            if invite_details:
                if invite_details.expiry_date > datetime.datetime.utcnow():
                    # ============================================== #
                    # Invite not expired then send link again
                    # ============================================== #
                    self.send_user_registration_link(invite_details)
                else:
                    # ============================================== #
                    # delete existing invite and send a new one
                    # ============================================== #
                    self.request.dbsession.delete(invite_details)
                    self.request.dbsession.flush()
                    invitation = self.create_registration_invitation(bus_email)
                    self.send_user_registration_link(invitation)
            else:
                # ============================================ #
                # send a new invite link
                # ============================================ #
                invitation = self.create_registration_invitation(bus_email)
                self.send_user_registration_link(invitation)

        return True

    def send_user_registration_link(self, invitation):
        bus_email = self.request.POST.get('bus_email')
        settings = self.request.registry.settings

        # ======================================= #
        # save user invitation data
        # ======================================= #
        invitation = self.create_registration_invitation(bus_email)
        to_address = bus_email
        from_address = settings['mailgun.from_email']
        reg_uuid = invitation.uuid
        url = self.request.route_url('cta_registration', token=reg_uuid)

        # ====================================== #
        # Creating email template body
        # ====================================== #
        body = render('cambium_cta:templates/email/registration.txt',
                      {'email': to_address, 'url': url},
                      request=self.request)

        # ===================================== #
        # Sending the mail to the user
        # ===================================== #
        mail_client = MailgunClient(self.request)
        mail_client.send_email(from_addr=from_address,
                               to_addrs=to_address,
                               subject="Cambium Total Access Registration",
                               body_text=body)
        return mail_client

    def send_fake_registration_mail(self):
        bus_email = self.request.POST.get('bus_email')
        settings = self.request.registry.settings

        to_address = bus_email
        from_address = settings['mailgun.from_email']

        # ====================================== #
        # Creating email template body
        # ====================================== #
        body = render('cambium_cta:templates/email/fake_registration.txt',
                      {},
                      request=self.request)

        # ===================================== #
        # Sending the mail to the user
        # ===================================== #
        mail_client = MailgunClient(self.request)
        mail_client.send_email(from_addr=from_address,
                               to_addrs=to_address,
                               subject="Cambium Total Access Registration",
                               body_text=body)
        return mail_client

    def create_registration_invitation(self, bus_email, came_from="cta"):
        invitation_object = UserInvites(email=bus_email,
                                        came_from=came_from)
        self.request.dbsession.add(invitation_object)
        self.request.dbsession.flush()
        return invitation_object

    def get_user_invitation_by_uuid(self, reg_uuid):
        return self.request.dbsession.query(UserInvites).filter_by(
            uuid=reg_uuid).first()

    def invitation_valid(self, invitation):
        if invitation.expiry_date > datetime.datetime.utcnow():
            if invitation.is_active:
                return True
        return False

    def register_user(self):
        import json
        form_values = self.request.params
        firstname = form_values.get('firstname')
        lastname = form_values.get('lastname')
        bus_email = form_values.get('bus_email')
        contact_num = form_values.get('contact_num')
        password = form_values.get('password')
        addr_1 = form_values.get('addr_1')
        addr_2 = form_values.get('addr_2')
        city = form_values.get('city')
        postal_code = form_values.get('postal_code')
        state = form_values.get('state')
        country = form_values.get('country')
        company = form_values.get('company_name')
        sms_checkbox = True if form_values.get('sms_checkbox') else False
        email_checkbox = True if form_values.get('email_checkbox') else False
        call_checkbox = True if form_values.get('call_checkbox') else False
        postal_checkbox = True if form_values.get('postal_checkbox') else False
        privacy_checkbox = True if form_values.get(
            'privacy_checkbox') else False

        user_object = User(email=bus_email,
                           first_name=firstname,
                           last_name=lastname,
                           password=password
                           )

        if "user_company_id" in self.request.session:
            user_object.company_id = self.request.session['user_company_id']

        self.request.dbsession.add(user_object)
        self.request.dbsession.flush()

        # import pdb; pdb.set_trace()

        invite = self.request.dbsession.query(UserInvites).filter_by(
            email=bus_email, is_active=True).first()
        if invite:
            invite.is_active = False
            self.request.dbsession.add(invite)
            self.request.dbsession.flush()

        # ======================================== #
        # Inserting the user profile details
        # ======================================== #
        country_id = self.request.dbsession.query(Country).filter_by(
            code=country).first().id
        # print(country_id)
        # import pdb; pdb.set_trace()
        # state_id = self.request.dbsession.query(State).filter_by(
        #     state_name=state).first()

        profile_object = UserProfile(
            user_id=user_object.id,
            user=user_object,
            phone_number=str(contact_num),
            street_address_1=addr_1,
            street_address_2=addr_2,
            town=city,
            state_id=int(state),
            postal_code=postal_code,
            comm_preferences=json.dumps({
                'sms': sms_checkbox,
                'email': email_checkbox,
                'call': call_checkbox,
                'postal': postal_checkbox
            }),
            terms_conditions=privacy_checkbox,
            country_id=int(country_id)

        )

        self.request.dbsession.add(profile_object)
        self.request.dbsession.flush()

        # # =======================================  #
        # # inserting data to UserActivityLogger
        # # =======================================  #
        # user_activity = UserActivityLogger(
        #     user_id=user_object.id,
        #     last_login = datetime.datetime.now(),
        #     login_success_count = 1,
        #     login_failure_count = 0
        #     )
        # self.request.dbsession.add(user_activity)
        # self.request.dbsession.flush()


        # ======================================= #
        # Code for amber road check
        # ======================================= #
        eod = geteod(self.request)

        try:
            response = eod.screenuser(user_object)
        except Exception as e:
            pass

        response.partnerid = user_object.uuid
        response = self.request.dbsession.merge(response)
        # event = RegistrationCompleteEvent(user_object, self.request)
        # self.request.registry.notify(event)
        return user_object


    def register_company(self):
        user_email = ""
        form_values = self.request.params

        company_name = form_values.get('company_name')
        company_number = form_values.get('company_number')
        company_website = form_values.get('company_website')
        company_address1 = form_values.get('company_address1')
        company_address2 = form_values.get('company_address2')
        company_country = form_values.get('company_country')
        company_state = form_values.get('company_state')
        company_zip = form_values.get('company_zip')
        company_city = form_values.get('company_city')

        if "email" in self.request.session:
            user_email = self.request.session['email']

        # =============================================== #
        # Checking whether the company already exist in
        # company table.
        # =============================================== #
        query = self.request.dbsession.query(Company)
        records = query.filter_by(
            postal_code=company_zip.strip(), name=company_name.strip(),
            url=company_website.strip(), contact_number=company_number.strip()
        ).all()

        #  code to get country id
        country_id = self.request.dbsession.query(Country).filter_by(
        code=company_country).first().id

        state_id = self.request.dbsession.query(State).filter_by(
        id=company_state).first().id

        if len(records) == 0:
            try:
                company_object = Company(
                    name=company_name,
                    contact_number=str(company_number),
                    url=company_website,
                    street_address_1=company_address1,
                    street_address_2=company_address2,
                    town=company_city,
                    state_id=state_id,
                    postal_code=company_zip,
                    email_domain=user_email.split('@')[-1],
                    country_id=country_id
                )
                self.request.dbsession.add(company_object)
                self.request.dbsession.flush()

                self.request.session['user_company_id'] = company_object.id
                return company_object
            except Exception as e:
                print (e)
                return {"server_error": True, "company_exists": False}
        else:
            return {"server_error": False, "company_exists": True}
