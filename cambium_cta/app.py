from pyramid.session import SignedCookieSessionFactory
my_session_factory = SignedCookieSessionFactory('itsaseekreet')


# ============================================= #
# Pyramid Session configuration
# ============================================= #
from pyramid.config import Configurator
config = Configurator()
config.set_session_factory(my_session_factory)

