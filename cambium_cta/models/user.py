import uuid
from datetime import datetime
from sqlalchemy import (
    Column,
    Boolean,
    DateTime,
    JSON,
    ForeignKey,
    String
)
from sqlalchemy.orm import relationship, synonym
from sqlalchemy.types import (
    Integer,
    Unicode,
    UnicodeText
)
from .company import Company
from .country import Country
from .state import State
from .meta import Base
from cambium_cta.lib.security import encryptpassword, verifypassword


def get_uuid():
    return str(uuid.uuid4())


class User(Base):
    """
       Application's user model.
    """
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, autoincrement=True, unique=True,
                index=True)
    uuid = Column(UnicodeText(256), default=get_uuid, nullable=False,
                  unique=True, index=True)
    email = Column(Unicode(120), unique=True, index=True)
    first_name = Column(Unicode(50))
    last_name = Column(Unicode(50))
    password_ = Column('password', String(128))

    @property
    def password(self):
        return self.password_

    @password.setter
    def password(self, password):
        self.password_ = encryptpassword(password)

    password = synonym('password_', descriptor=password)
    joined_date = Column(DateTime, default=datetime.utcnow())
    company_id = Column(Integer, ForeignKey('company.id'))
    company = relationship(Company, uselist=False)
    invalid_login_count = Column(Integer, default=0)
    is_account_lock = Column(Boolean, default=False)
    is_active = Column(Boolean, default=True)
    profile = relationship("UserProfile")

    def __str__(self):
        return "<User(id=%s)>" % self.id

    def verify_password(self, password):
        return verifypassword(password, self.password)

    def _maybegetprofile(self):
        # Want to load structuredaddress property if it exists, but
        # don't want to trigger a db query if it doesn't. First check
        # to see if it's already loaded (this is necessary in case the
        # object hasn't been flushed to the database):
        if self.__dict__.get('profile') is not None:
            return self.profile.street_address_1
        # Otherwise, if addressid is not null, we can trigger a lazy load
        if self.profile_id is not None:
            return self.profile.street_address_1

    @property
    def address(self):
        profiledata = self._maybegetprofile()
        if profiledata is not None:
            return unicode(profiledata)
        return self.profiledata.street_address_1

    @property
    def country(self):
        profiledata = self._maybegetprofile()
        if profiledata is not None:
            return profiledata.country


class UserProfile(Base):
    __tablename__ = 'user_profile'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship(User, uselist=False, backref="user_profile")
    phone_number = Column(String(13))
    street_address_1 = Column(UnicodeText(200))
    street_address_2 = Column(UnicodeText(200))
    town = Column(UnicodeText(80))
    state_id = Column(Integer, ForeignKey('state.id'))
    state = relationship(State)
    postal_code = Column(UnicodeText(9))
    country_id = Column(Integer, ForeignKey('country.id'))
    country = relationship(Country)
    # country = Column(UnicodeText(80))
    comm_preferences = Column(JSON)
    terms_conditions = Column(Boolean, default=False)
    avatar = Column(Unicode(80))

    def __str__(self):
        return "<UserProfile(id=%s)>" % self.id

class UserActivityLogger(Base):
    __tablename__ = 'user_activity_logger'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship(User, uselist=False)
    ip_address = Column(UnicodeText(25))
    browser_user_agent = Column(String(50))
    last_login = Column(DateTime)
    login_success_count = Column(Integer)
    login_failure_count = Column(Integer)
