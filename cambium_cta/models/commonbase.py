import uuid
from datetime import datetime

from sqlalchemy import (Column, DateTime, UnicodeText)

from .meta import Base


def get_uuid():
    return str(uuid.uuid4())


""" This is common abstract calss that can be used by 
all the other model classes """


class CommonBase(Base):
    __abstract__ = True
    uuid = Column(UnicodeText(256), default=get_uuid, nullable=False,
                  unique=True, index=True)
    created_datetime = Column(DateTime, default=datetime.utcnow)
    modified_datetime = Column(DateTime, default=datetime.utcnow)
