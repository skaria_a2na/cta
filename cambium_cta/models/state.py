from sqlalchemy import (
    Column,
    Integer,
    Text,
    Unicode,
    UnicodeText,
    String
)

from .meta import Base

from sqlalchemy.orm import relationship

class State(Base):
    __tablename__ = 'state'
    id = Column(Integer, primary_key=True, autoincrement=True,
                index=True)
    state_name = Column(Unicode(255), nullable=False)
    country_code = Column(Text)
    mysql_charset='utf8mb4'
