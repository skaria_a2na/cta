from sqlalchemy import (
    Column,
    Text,
    ForeignKey,
    String
)

from sqlalchemy.types import (
    Integer,
    Unicode,
    UnicodeText,
)

from .commonbase import CommonBase
from sqlalchemy.orm import relationship
from cambium_cta.models.country import Country
from cambium_cta.models.state import State

class Company(CommonBase):
    __tablename__ = 'company'
    id = Column(Integer, primary_key=True, autoincrement=True, unique=True,
                index=True)
    name = Column(String(100))
    email_domain = Column(Unicode(120), index=True)
    contact_number = Column(String(15))
    url = Column(Text(120))
    street_address_1 = Column(UnicodeText(200))
    street_address_2 = Column(UnicodeText(200))
    town = Column(UnicodeText(80))
    state_id = Column(Integer, ForeignKey('state.id'))
    state = relationship(State)
    postal_code = Column(UnicodeText(9))
    country_id = Column(Integer, ForeignKey('country.id'))
    country = relationship(Country)
    mysql_charset='utf8mb4'


    def __str__(self):
        return "<Company(id=%s)>" % self.id
