import uuid
from datetime import datetime

import sqlalchemy as sa
import sqlalchemy.orm as saorm

from .user import User
from .commonbase import CommonBase


def get_uuid():
    return str(uuid.uuid4())


class DeniedPartyScreeningStatus(CommonBase):
    __tablename__ = 'denied_party_screening'
    partnerid = sa.Column(sa.String(64), primary_key=True, nullable=False,
                  unique=True, index=True)
    uuid = sa.Column(sa.String(256), default=get_uuid, nullable=False,
                  unique=True, index=True)
    email = sa.Column(sa.String(250), unique=True, index=True)
    match = sa.Column(sa.String(64))
    embargohold = sa.Column(sa.String(8))
    customembargomatch = sa.Column(sa.String(8))
    statuscode = sa.Column(sa.Integer)
    statusdescription = sa.Column(sa.Text())
    from_application = sa.Column(sa.String(50))
    screening_status = sa.Column(sa.Boolean, default=True)
    last_screened_date = sa.Column(sa.DateTime, default=datetime.utcnow())

    user = saorm.relationship(
        User,
        primaryjoin=(saorm.foreign(partnerid) == User.id),
        backref=saorm.backref('denied_party_screening', uselist=False),
    )

    # =============================================== #
    #Todo : Add fields for company details
    # =============================================== #

    @property
    def ok(self):
        return self.statuscode == 1000
