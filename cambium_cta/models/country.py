from sqlalchemy import (
    Column,
    Integer,
    Text,
    Unicode,
    UnicodeText,
    String,
)

from .meta import Base

from sqlalchemy.orm import relationship

class Country(Base):
    __tablename__ = 'country'
    id = Column(Integer, primary_key=True, autoincrement=True,
                index=True)
    name = Column(Unicode(255), unique=True, nullable=False)
    code = Column(Text, index=True)
