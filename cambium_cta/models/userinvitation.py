from datetime import datetime, timedelta

from sqlalchemy import (
    Column,
    Boolean,
    DateTime
)
from sqlalchemy.types import (
    Integer,
    Unicode
)

from .commonbase import CommonBase


def utc_datetime_tomorrow():
    return datetime.utcnow() + timedelta(days=1)


class UserInvites(CommonBase):
    __tablename__ = 'user_invites'
    id = Column(Integer, primary_key=True, autoincrement=True, unique=True,
                index=True)
    email = Column(Unicode(120), nullable=False)
    is_active = Column(Boolean, default=True)
    #for_registration = Column(Boolean, default=False)
    #for_password_reset = Column(Boolean, default=False)
    came_from = Column(Unicode(120))
    expiry_date = Column(DateTime, default=utc_datetime_tomorrow)


class PasswordResetInvites(CommonBase):
    __tablename__ = 'password_reset_invites'
    id = Column(Integer, primary_key=True, autoincrement=True, unique=True,
                index=True)
    email = Column(Unicode(120), nullable=False)
    is_active = Column(Boolean, default=True)
    #for_registration = Column(Boolean, default=False)
    #for_password_reset = Column(Boolean, default=False)
    came_from = Column(Unicode(120))
    expiry_date = Column(DateTime, default=utc_datetime_tomorrow)
